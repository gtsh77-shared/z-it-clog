package tools

import (
	"crypto/rand"
	"encoding/hex"
)

func RandHex(bytes int) (string, error) {
	var (
		hash []byte = make([]byte, bytes)
		err  error
	)

	if _, err = rand.Read(hash); err != nil {
		return "", err
	}

	return hex.EncodeToString(hash), nil
}
