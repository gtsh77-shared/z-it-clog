package tools

import (
	"strings"

	"github.com/go-playground/validator/v10"
)

type ValidationError struct {
	Code    int                 `json:"code"`
	Message string              `json:"message"`
	Fields  []*ValidationFields `json:"fields"`
}

type ValidationFields struct {
	Field       string `json:"name"`
	Description string `json:"description"`
}

func NewValidationError(code int, msg string) *ValidationError {
	return &ValidationError{
		Code:    code,
		Message: msg,
		Fields:  make([]*ValidationFields, 0),
	}
}

func DescribeValidationErrors(code int, msg string, errs interface{}) *ValidationError {
	var (
		ok              bool
		validationError *ValidationError = NewValidationError(code, msg)

		err validator.FieldError
	)

	if errs, ok = errs.(validator.ValidationErrors); ok {
		for _, err = range errs.(validator.ValidationErrors) {
			validationError.Fields = append(validationError.Fields, &ValidationFields{Field: err.Field(), Description: err.Error()[strings.Index(err.Error(), "Error"):]})
		}
	}

	return validationError
}
