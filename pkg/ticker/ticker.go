package ticker

import (
	"time"
)

type Ticker struct {
	t  *time.Ticker
	i  time.Duration
	id string
	sc chan bool
	f  func() error
	ef func(error)
}

func New(
	id string,
	i time.Duration,
	f1 func() error,
	f2 func(error)) *Ticker {
	return &Ticker{
		id: id,
		i:  i,
		f:  f1,
		ef: f2,
	}
}

type ITicker interface {
	Enable()
	Disable()
	start()
}

func (t *Ticker) Enable() {
	t.sc = make(chan bool)
	t.t = time.NewTicker(t.i)

	go t.start()
}

func (t *Ticker) Disable() {
	t.sc <- true
}

func (t *Ticker) start() {
	var err error

	defer t.t.Stop()

	for {
		select {
		case <-t.sc:
			return
		case <-t.t.C:
			if err = t.f(); err != nil {
				t.ef(err)
			}
		}
	}
}
