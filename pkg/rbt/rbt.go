package rbt

import (
	"sync"
)

// ******************************
// ******* RED BLACK TREE *******
// ******* DATA STRUCTURE *******
// **** LIMITED (ADD, SEARCH) ****
// ******************************

type IRBT interface {
	Size() uint32
	Height(*RBTNode) uint8
	Search(k uint32) (uint, *interface{}, bool)
	Add(uint, interface{}) *RBTNode
	AppendNodes(*RBTNode) []*DLLNode[*uint32, interface{}]
	AppendDataByKey(uint32) []*interface{}
}

type RBT struct {
	d *DLL[*uint32, interface{}]

	r *RBTNode
	s uint32
	l sync.Mutex
}

type RBTNode struct {
	l, r, p *RBTNode
	isRed   bool

	n *DLLNode[*uint32, interface{}]
}

func NewRBT() *RBT {
	return &RBT{
		d: new(DLL[*uint32, interface{}]),
	}
}

func (r *RBT) Size() uint32 {
	return r.s
}

func (r *RBT) Height(n *RBTNode) uint8 {
	var lc, rc uint8

	if n == nil {
		return 0
	}

	lc = r.Height(n.l)
	rc = r.Height(n.r)

	if lc > rc {
		return lc + 1
	} else {
		return rc + 1
	}
}

func (r *RBT) AppendNodes(n *RBTNode) []*DLLNode[*uint32, interface{}] {
	var res []*DLLNode[*uint32, interface{}] = make([]*DLLNode[*uint32, interface{}], 0)

	r.appendNodeRec(n, &res)

	return res
}

func (r *RBT) appendNodeRec(n *RBTNode, res *[]*DLLNode[*uint32, interface{}]) {
	if n == nil {
		return
	}

	r.appendNodeRec(n.l, res)
	if n.n.k != nil {
		*res = append(*res, n.n)
	}
	r.appendNodeRec(n.r, res)
}

func (r *RBT) AppendDataByKey(k uint32) []*interface{} {
	var (
		n *RBTNode
		b bool

		res []*interface{} = make([]*interface{}, 0)
	)

	if n, b = r.searchNode(k, r.r); !b {
		return nil
	}

	r.appendDataByKeyRec(n, k, &res)

	return res
}

func (r *RBT) appendDataByKeyRec(n *RBTNode, k uint32, res *[]*interface{}) {
	if n == nil {
		return
	}

	r.appendDataByKeyRec(n.l, k, res)
	if n.n.k != nil && *n.n.k == k {
		*res = append(*res, n.n.v)
	}
	r.appendDataByKeyRec(n.r, k, res)
}

func (r *RBT) Search(k uint32) (uint32, *interface{}, bool) {
	var (
		n *RBTNode
		b bool
	)

	if n, b = r.searchNode(k, r.r); !b {
		return 0, nil, false
	}

	return *n.n.k, n.n.v, true
}

func (r *RBT) searchNode(k uint32, n *RBTNode) (*RBTNode, bool) {
	if r.Size() == 0 {
		return nil, false
	}

	if n.n.k != nil {
		if *n.n.k == k {
			return n, true
		} else if *n.n.k < k {
			if n.r.n.k != nil {
				return r.searchNode(k, n.r)
			}
		} else {
			if n.l.n.k != nil {
				return r.searchNode(k, n.l)
			}
		}
	}

	return n, false
}

func (r *RBT) searchSlot(k uint32) *RBTNode {
	var n *RBTNode

	if r.Size() == 0 {
		return nil
	}

	n = r.r
	for n != nil {
		if n.n.k != nil {
			if *n.n.k < k {
				n = n.r
			} else {
				n = n.l
			}
		} else {
			return n
		}
	}

	return n
}

func (r *RBT) Add(k uint32, v interface{}) *RBTNode {
	var p, nn, lc, rc *RBTNode

	r.l.Lock()
	defer r.l.Unlock()

	lc = &RBTNode{
		n: r.d.PushBack(nil, nil),
	}
	rc = &RBTNode{
		n: r.d.PushBack(nil, nil),
	}

	if p = r.searchSlot(k); p == nil {
		nn = &RBTNode{
			n: r.d.PushBack(&k, v),
			l: lc,
			r: rc,
		}
		r.r = nn
	} else {
		nn = p
		nn.isRed = true
		nn.l = lc
		nn.r = rc
		nn.n.k = &k
		nn.n.v = &v
	}
	lc.p = nn
	rc.p = nn

	r.s++
	r.balanceIN(nn)

	return nn
}

func (r *RBT) balanceIN(n *RBTNode) {
	var nn *RBTNode

	for n != r.r && n.p.isRed {
		if n.p == n.p.p.l {
			nn = n.p.p.r
			if nn.isRed {
				nn.isRed = false
				n.p.isRed = false
				n.p.p.isRed = true
				n = n.p.p
			} else {
				if n == n.p.r {
					n = n.p
					r.rotateLeft(n)
				}

				n.p.isRed = false
				n.p.p.isRed = true
				r.rotateRight(n.p.p)
			}
		} else {
			nn = n.p.p.l
			if nn.isRed {
				n.p.isRed = false
				nn.isRed = false
				n.p.p.isRed = true
				n = n.p.p
			} else {
				if n == n.p.l {
					n = n.p
					r.rotateRight(n)
				}

				n.p.isRed = false
				n.p.p.isRed = true
				r.rotateLeft(n.p.p)
			}
		}
	}
	r.r.isRed = false
}

func (r *RBT) rotateLeft(n *RBTNode) {
	var nr *RBTNode = n.r

	n.r = nr.l
	if nr.l != nil {
		nr.l.p = n
	}
	nr.p = n.p
	if n.p != nil {
		if n.p.l == n {
			n.p.l = nr
		} else {
			n.p.r = nr
		}
	} else {
		r.r = nr
	}

	nr.l = n
	if n != nil {
		n.p = nr
	}
}

func (r *RBT) rotateRight(n *RBTNode) {
	var nl *RBTNode = n.l

	n.l = nl.r
	if nl.r != nil {
		nl.r.p = n
	}
	nl.p = n.p
	if n.p != nil {
		if n.p.r == n {
			n.p.r = nl
		} else {
			n.p.l = nl
		}
	} else {
		r.r = nl
	}

	nl.r = n
	if n != nil {
		n.p = nl
	}
}

// ***** DOUBLE LINKED LIST *****
// ******* DATA STRUCTURE *******
// ***** LIMITED (PUSHBACK) *****
// ******************************

type IDDL[K comparable, T interface{}] interface {
	Size() uint
	Empty() bool
	PushBack(K, T) *DLLNode[K, T]
}

type DLL[K comparable, T interface{}] struct {
	l    sync.Mutex
	s    uint
	h, t *DLLNode[K, T]
}
type DLLNode[K comparable, T interface{}] struct {
	p, n *DLLNode[K, T]

	k K
	v *T
}

func (l *DLL[K, T]) Size() uint {
	return l.s
}

func (l *DLL[K, T]) Empty() bool {
	return l.t == nil
}

func (l *DLL[K, T]) PushBack(k K, v T) *DLLNode[K, T] {
	var n *DLLNode[K, T] = &DLLNode[K, T]{
		k: k,
		v: &v,
	}

	l.l.Lock()
	defer l.l.Unlock()

	if l.Empty() {
		l.h = n
		l.t = n
	} else {
		n.p = l.t
		l.t.n = n
		l.t = n
	}

	l.s++

	return n
}
