package autotest

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	neturl "net/url"
	"os"
	"path"
	"runtime"
	"strings"
	"testing"

	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/service"
)

const (
	ConstTestToken = "autotest"
)

type AT struct {
	Config   *ATConfig
	Client   *RequestClient
	TestCase *TestCase
}

type ATConfig struct {
	AuthGroup bool
}

type RequestClient struct {
	CTX        context.Context
	App        *service.Service
	TestServer *httptest.Server
	Tokens     map[string]string
	BaseURL    string
}

type TestCase struct {
	URL                           string
	Description                   string
	Method                        string
	Request                       string
	Response                      string
	Cookies                       []*http.Cookie
	Status                        int
	ExpectedStatus                int
	ExpectedResponse              string
	ExpectedResponseShouldContain map[string]string
	MustHaveJSONFields            map[string]interface{}
	PreAction                     func(*AT) error
	PostAction                    func(*AT, string, []*http.Cookie) error
	RefreshTokenInRequest         bool
	Headers                       map[string]string
}

type Opts struct {
	Key   string
	Value interface{}
}

type IAT interface {
	GetInstance() *service.Service
	ResetDB() error
	ResetRedis() error
	DoTest(int, *testing.T, *TestCase, ...Opts)
	Request(string, string, string, map[string]string) (int, []byte, []*http.Cookie, error)
}

func New(config *ATConfig) *AT {
	return &AT{
		Config: config,
		Client: &RequestClient{
			CTX:    context.Background(),
			Tokens: make(map[string]string),
		},
	}
}

func (at *AT) GetInstance() *service.Service {
	var err error

	if at.Client.App, err = service.New("", "", "").Start(); err != nil {
		log.Fatal(err)
	}

	return nil
}

func (at *AT) Request(method, json, u string, headers map[string]string) (int, []byte, []*http.Cookie, error) {
	var (
		url     string
		nURL    *neturl.URL
		rec     *http.Request
		res     *http.Response
		payload []byte
		err     error
	)

	if nURL, err = neturl.Parse(fmt.Sprint(at.Client.TestServer.URL, u)); err != nil {
		return 0, payload, nil, err
	}
	url = nURL.String()

	if rec, err = http.NewRequestWithContext(at.Client.CTX, method, url, strings.NewReader(json)); err != nil {
		return 0, payload, nil, err
	}

	rec.Header.Add("Content-Type", "application/json")

	if at.Config.AuthGroup {
		rec.Header.Add("Authorization", fmt.Sprint("Basic ", ConstTestToken))
	}

	if len(headers) != 0 {
		for k, v := range headers {
			rec.Header.Add(k, v)
		}
	}

	if res, err = http.DefaultClient.Do(rec); err != nil {
		return 0, payload, nil, err
	}

	defer res.Body.Close()

	if payload, err = io.ReadAll(res.Body); err != nil {
		return 0, payload, nil, err
	}

	return res.StatusCode, payload, res.Cookies(), nil
}

func (at *AT) DoTest(index int, t *testing.T, c *TestCase, opts ...Opts) { //nolint:gocognit // ack
	var (
		op      map[string]interface{} = make(map[string]interface{})
		IsEqual bool
		payload []byte
		j, el   *JQ
		cm      map[string]interface{}
		fname   string
		err     error
	)

	_, fname, _, _ = runtime.Caller(0)
	if err = os.Chdir(path.Join(path.Dir(fname), "../..")); err != nil {
		t.Fatalf("cant set dir %v", err)
	}

	if len(c.Request) > 0 {
		t.Logf("Testing [%s] with payload: %s\n", c.Description, c.Request)
	} else {
		t.Logf("Testing [%s]\n", c.Description)
	}

	for _, v := range opts {
		op[v.Key] = v.Value
	}

	if at.Client.App == nil {
		at.GetInstance()
	}

	if at.Client.TestServer != nil {
		at.Client.TestServer.Close()
	}

	at.Client.TestServer = httptest.NewServer(at.Client.App.T)

	if c.RefreshTokenInRequest {
		c.Request = `{"refresh_token":"` + at.Client.Tokens["refresh_token"] + `"}`
	}

	if c.PreAction != nil {
		if err = c.PreAction(at); err != nil {
			t.Errorf("%v: %v: pre action err: %v", c.Description, index, err)
		}
	}

	if c.Status, payload, c.Cookies, err = at.Request(c.Method, c.Request, c.URL, c.Headers); err != nil {
		t.Errorf("%v: %v: err: %v", c.Description, index, err)
	}

	c.Response = strings.TrimSpace(string(payload))
	c.ExpectedResponse = strings.TrimSpace(c.ExpectedResponse)

	if c.PostAction != nil {
		if err = c.PostAction(at, c.Response, c.Cookies); err != nil {
			t.Errorf("%v: %v: post action err: %v", c.Description, index, err)
		}
	}

	if c.ExpectedStatus != 0 && c.ExpectedStatus != c.Status {
		t.Errorf("%v: invalid http status: %d (%s), expected: %d (%s)", c.Description, c.Status, http.StatusText(c.Status), c.ExpectedStatus, http.StatusText(c.ExpectedStatus))
	}

	if c.ExpectedResponse != "" {
		if IsEqual, err = NewJSONEquilator(c.Response).FullEQ(c.ExpectedResponse); err != nil {
			t.Errorf("jsonsEqual: %v, test: %v", err, c.Description)
		}

		if !IsEqual {
			t.Errorf("%v: %v:\nresponse should be:\n%v\nbut it is:\n%v\n", c.Description, index, c.ExpectedResponse, c.Response)
		}
	}

	if c.MustHaveJSONFields != nil {
		cm = make(map[string]interface{})
		if err = json.Unmarshal(payload, &cm); err != nil {
			t.Fatal(err)
		}

		j = NewJSONEquilator(cm)
		for k, v := range c.MustHaveJSONFields {
			el = j.Path(k)
			if !el.Eq(v) {
				t.Errorf("%v: %v: response should have %s = %v, but it is %v", c.Description, index, k, v, el.JSON)
			}
		}
	}

	t.Log("OK")
}
