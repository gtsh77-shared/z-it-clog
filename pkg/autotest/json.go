package autotest

import (
	"encoding/json"
	"reflect"
	"strconv"
	"strings"
)

type JQ struct {
	JSON interface{}
}

type IJQ interface {
	Null() *JQ
	Path(string) *JQ
	Query(...string) *JQ
	Check(string) *JQ
	Eq(interface{}) bool
	FullEQ(interface{}) (bool, error)
}

func NewJSONEquilator(JSON interface{}) *JQ {
	return &JQ{
		JSON: JSON,
	}
}

func (j *JQ) Null() *JQ {
	return &JQ{JSON: nil}
}

func (j JQ) Path(p string) *JQ {
	return j.Query(strings.Split(p, ".")...)
}

func (j *JQ) Query(ps ...string) *JQ {
	c := j

	for _, p := range ps {
		c = c.Check(p)
	}

	return c
}

func (j *JQ) Check(p string) *JQ {
	if j == nil {
		return nil
	}

	if o, ok := j.JSON.(*interface{}); ok {
		j.JSON = *o
	}

	switch o := j.JSON.(type) {
	case map[string]interface{}:
		r, ok := o[p]
		if !ok {
			return j.Null()
		}
		return &JQ{JSON: r}

	case []interface{}:
		n, err := strconv.Atoi(p)
		if err != nil {
			if p == "#" {
				return &JQ{JSON: len(o)}
			}
			return j.Null()
		}
		if n >= cap(o) {
			return j.Null()
		}
		r := o[n]
		return &JQ{JSON: r}
	}

	return j.Null()
}

func (j JQ) Eq(r interface{}) bool {
	if j.JSON == nil {
		return false
	}
	switch lv := j.JSON.(type) {
	case float64:
		if rv, ok := r.(float64); ok {
			return lv == rv
		}
	case int:
		if rv, ok := r.(int); ok {
			return lv == rv
		}
	case string:
		if rv, ok := r.(string); ok {
			return lv == rv
		}
	case bool:
		if rv, ok := r.(bool); ok {
			return lv == rv
		}
	}

	return false
}

func (j *JQ) FullEQ(jsonStr interface{}) (bool, error) {
	var (
		json1, json2 interface{}
		err          error
	)

	if err = json.Unmarshal([]byte(j.JSON.(string)), &json1); err != nil {
		return false, err
	}

	if err = json.Unmarshal([]byte(jsonStr.(string)), &json2); err != nil {
		return false, err
	}

	return reflect.DeepEqual(json1, json2), nil
}
