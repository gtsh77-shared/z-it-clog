#!make
include deploy/clog/env/local.env
export $(shell sed 's/=.*//' ./deploy/clog/env/local.env)

V := @

RELEASE=$(shell git describe --always --tags)
BUILD_TIME?=$(shell date '+%F_%T%z')

OUT_DIR := ./bin

APP := "clog"

default: all

.PHIONY: all
all:clog

.PHONY: linux
linux: export GOOS := linux
linux: export GOARCH := amd64
linux:all


.PHONY: clog
clog: APP_OUT := $(OUT_DIR)/$(APP)
clog: APP_MAIN := ./cmd/clog
clog:
	@echo BUILDING $(APP_OUT)
	$(V)go build -ldflags "-s -w -X main.name=${APP} -X main.version=${RELEASE} -X main.compiledAt=${BUILD_TIME}" -o $(APP_OUT) $(APP_MAIN)
	@echo DONE

run:
	$(OUT_DIR)/${APP}

test:
	@echo "Running autotests..."
	$(V)go clean -testcache
	$(V)go test -p 1 ./...

lint:
	@echo "Running golangci-lint..."
	$(V)golangci-lint run

HUB := "z-it"
.PHONY: image
image: linux
image:
	docker image build -t ${HUB}/${APP}:${RELEASE} -t ${HUB}/${APP}:latest -f deploy/$(APP)/Dockerfile .
