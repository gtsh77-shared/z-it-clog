package config

import (
	"net"
	"time"
)

type TLS struct {
	Enabled   bool
	CrtPath   string
	KeyPath   string
	CrtCAPath string
}

type Config struct {
	Runtime struct {
		ID         string // авто-генерация
		Name       string // LD флаг
		Version    string // LD флаг
		CompiledAt string // LD флаг
		LogLevel   int    `conf:"default:0"`
		LogAsJSON  bool   `conf:"default:true"`
	}
	Net struct {
		Addr               net.IP        // авто-определение по интерфейсу
		DomainNames        []string      `conf:"default:*"`
		InterfaceName      string        `conf:"default:eth0"`
		InterfaceAddrIndex int           `conf:"default:0"`
		HTTPPort           int           `conf:"default:80"`
		HTTPTimeout        time.Duration `conf:"default:30s"`
		EnableProm         bool          `conf:"default:true"`
		MetricPath         string        `conf:"default:/metrics"`
		HealthPath         string        `conf:"default:/health/check"`
		TLS                *TLS
	}
	Processor struct {
		FilePath     string
		FilePathAt   string
		PollInterval time.Duration
		BatchSize    int

		FixtureEnabled    bool
		FixtureFilePath   string
		FixtureLinesCount int
		FixtureUsersCount int
	}
}

func New(name, version, compiledAt string) *Config {
	var config Config

	config.Runtime.Name = name
	config.Runtime.Version = version
	config.Runtime.CompiledAt = compiledAt

	return &config
}
