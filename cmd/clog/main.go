package main

import (
	"log"

	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/service"
)

var name, version, compiledAt string //nolint:gochecknoglobals //lld flags

func main() {
	if _, err := service.New(name, version, compiledAt).Start(); err != nil {
		log.Fatal(err)
	}
}
