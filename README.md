# CLOG v1.0

#### требования
go v1.20+, docker

#### документация
[OpenAPI](https://gitlab.com/gtsh77-shared/z-it-clog/-/blob/main/docs/openapi.yml)

#### сборка

```
make
```

#### запуск clog на интерфейсе loopback (linux)
```
make run
```

#### установка линтера (linux, опционально)
```
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.55.1
```

#### запуск линтера
```
make lint
```

#### запуск API автотестов
```
make test
```

#### добавление новых строк лога во внешнее хранилище
```
printf "\n5, 127.0.0.1, 17:57:59" >> data/conn_log
```

#### альтернативный запуск 1 (linux): [отредактировать env файл](https://gitlab.com/gtsh77-shared/z-it-clog/-/blob/main/deploy/clog/env/local.env), экспортировать переменные в vtty, проверить, запустить clog
```
vi deploy/env/local.env
export $(grep -v '^#' ./deploy/env/local.env | xargs) 
env |grep CLOG
./bin/clog
```

#### альтернативный запуск 2 (windows): сборка docker образа, запуск контейнера с маунтом внешнего хранилища
```
make image
docker run --rm --name=clog -v ./data:/app/clog/data --env-file=deploy/clog/env/local.env -e CLOG_NET_INTERFACE_NAME=eth0 -p 12345:12345/tcp -it z-it/clog:latest
```

#### проверка доступности экземпляра
```
curl 0.0.0.0:12345/health/check
```

#### метрики prometheus (runtime, http)
```
curl 0.0.0.0:12345/metrics
```


####  тестовые данные
в директории data представлен файл conn_log_randoms_1m_100k.tar.gz, содержащий 1M строк с 100к уникальных user_id и 10к уникальных IPv4:

1. распаковать 
```
tar xfz data/conn_log_randoms_1m_100k.tar.gz -C data
```
2. изменить env переменную
```
CLOG_PROCESSOR_FILE_PATH=./data/conn_log_randoms_1m_100k
```

3. перезапустить clog
4. выполнить тест запросы
```
curl 0.0.0.0:12345/20039/3182 - {"dupes":true}
curl 0.0.0.0:12345/20039/52741 - {"dupes":false}
```

>альтернативно загрузить conn_log_randoms_100m_100k.tar.gz, содержащий 100M строк с 100к уникальных user_id и 10к уникальных IPv4 https://disk.yandex.ru/d/qTNetXOYT5TTmw


#### генератор фикстур (опционально)
в clog есть встроенный генератор фикстур, для его использование необходимо изменить env

```
CLOG_PROCESSOR_FIXTURE_ENABLED=true
CLOG_PROCESSOR_FIXTURE_FILE_PATH=./data/filename
CLOG_PROCESSOR_FIXTURE_LINES_COUNT=количество_строк
CLOG_PROCESSOR_FIXTURE_USERS_COUNT=количество_уникальных_пользователей
```

#### нагрузочное тестирование
[![image](https://i.postimg.cc/4ymVwD9T/Bildschirmfoto-2023-10-30-05-32-46.png)](https://i.postimg.cc/4ymVwD9T/Bildschirmfoto-2023-10-30-05-32-46.png)
>экземпляр clog и 100M записей тестировался при нагрузке 15-20k RPS, макс задержка ответа составила 5ms

## Архитектура 

#### cтруктура данных
1. внешнее хранилище - текстовый файл в директории data, задается через env , поддерживается добавление новых строк
2. [красно-черное](https://ru.wikipedia.org/wiki/%D0%9A%D1%80%D0%B0%D1%81%D0%BD%D0%BE-%D1%87%D1%91%D1%80%D0%BD%D0%BE%D0%B5_%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D0%BE) бинарное дерево поиска на базе связного списка с вершинами ключей множества user_id. Используются для быстрой вставки и балансировки новых ключей без операций перевыделения памяти, а также для выполнения поиска O(log(n))

[![image](https://i.postimg.cc/MGnkDHKp/rbt-50i.png)](https://i.postimg.cc/Xn8VnBRd/rbt-50i.png)
>структура сбалансированного дерева для первых 50 user_id из сета conn_log_randoms_1m_100k

3. [xsync](https://github.com/puzpuzpuz/xsync).mapOf[uint32][]int64 на базе [Cache-Line Hash Table](https://github.com/LPD-EPFL/CLHT). Используется для значительного уменьшения количества циклов CPU необходимых для безопасного выполнения операции вставки/удаления

[![image](https://i.postimg.cc/0jmk6f0f/Ag006imd-D.png)](https://i.postimg.cc/0jmk6f0f/Ag006imd-D.png)
>бенчмарк 1M/s записей/удалений (красный цвет - sync.Map, зеленый - xsync.MapOf, меньше - лучше)


#### описание работы компонента: процессор внешнего хранилища
тикер каждые `CLOG_PROCESSOR_POLL_INTERVAL` проверяет файл на предмет изменения размера, если он увеличился, то запускает функцию построчной обработки новых `CLOG_PROCESSOR_BATCH_SIZE` строк, каждая итерация обработки передает обработанные данные в хранилище, где в дерево поиска добавляется новая вершина и/или новый элемент `xsync.mapOf`, содержащий ключ - ipv4 адрес представленный `uint32` и значение - `[]int64`, где новые TS элементы добавляются в хвост

#### описание работы эндпоинта REST API: GET /:uid1/:uid2
входящий http запрос на порт `CLOG_NET_HTTP_PORT` валидируется, далее происходит внутренний запрос в хранилище c передачей переменных uid1, uid2 из запроса для получения в ответе признака наличия дубликата, хранилище выполняет поиск по дереву двух элементов `xsync.mapOf` по ключам uid1, uid2 (сложность: O(log(n)), далее происходит перебор элементов `xsync.mapOf` с адресами пользователя А (сложность: O(n)), где в каждой итерации происходит проверка наличия ключа среди элементов `xsync.mapOf` с адресами пользователя Б (сложность: O(1)), при первом пересечении выполняется перебор `[]int64` данных TS адреса пользователя А (O(n)), каждый элемент которого ищется в `[]int64` данных TS адреса пользователя Б, используя метод бинарного поиска (O(log(n))), далее если одна из итераций поиска возвращает индекс >= 0, то признак дубликата возвращается в обработчик http запроса, где формируется json согласно структуре ответа и возвращается клиенту с кодом `200`. Код `503` возвращается при достижении порога `CLOG_NET_HTTP_TIMEOUT`

#### внешние библиотеки

|                        |                                               |
| ---------------------- | --------------------------------------------- |
| логгер                 | https://pkg.go.dev/go.uber.org/zap          |
| env/args парсер/конфиг | https://github.com/ardanlabs/conf           |
| http фреймворк         | https://github.com/labstack/echo            |
| валидатор              | https://github.com/go-playground/validator  |
| пром метрики            | https://github.com/prometheus/client_golang |
| быстрый map, mutex     | https://github.com/puzpuzpuz/xsync          |

