package router

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type Validator struct {
	v *validator.Validate
}

const (
	constNumberTwo = 2
)

func (v *Validator) Validate(i interface{}) error {
	var err error

	if err = v.v.Struct(i); err != nil {
		return err
	}

	return nil
}

func IsRFC3339(fl validator.FieldLevel) bool {
	var (
		ISO8601DateRegexString string
		ISO8601DateRegex       *regexp.Regexp
	)

	ISO8601DateRegexString = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])(?:T|\\s)(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9]).([0-9][0-9][0-9]Z)$"
	ISO8601DateRegex = regexp.MustCompile(ISO8601DateRegexString)

	return ISO8601DateRegex.MatchString(fl.Field().String())
}

func EnableCustomValidation(e *echo.Echo) error {
	var (
		vv  *validator.Validate = validator.New()
		err error
	)

	// custom validator injected w json tag representation
	vv.RegisterTagNameFunc(func(f reflect.StructField) string {
		var name string

		if name = strings.SplitN(f.Tag.Get("json"), ",", constNumberTwo)[0]; name == "-" {
			return ""
		}

		return name
	})

	if err = vv.RegisterValidation("RFC3339", IsRFC3339); err != nil {
		return err
	}

	e.Validator = &Validator{v: vv}

	return nil
}
