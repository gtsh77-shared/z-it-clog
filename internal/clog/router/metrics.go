package router

import (
	eprom "github.com/globocom/echo-prometheus"
	"github.com/labstack/echo/v4"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func EnableMetrics(e *echo.Echo, path string) {
	// add prom metrics
	e.Use(eprom.MetricsMiddlewareWithConfig(eprom.Config{
		Namespace: "http",
		Buckets: []float64{
			0.001, // 1ms
			0.002, // 2ms
			0.003, // 3ms
			0.004, // 4ms
			0.005, // 5ms
			0.01,  // 10ms
			0.02,  // 20ms
			0.03,  // 30ms
			0.04,  // 40ms
			0.05,  // 50ms
			0.1,   // 100ms
			0.25,  // 250ms
			0.5,   // 500ms
			1,     // 1s
			5,     // 5s
			10,    // 10s
		},
	}))
	e.GET(path, echo.WrapHandler(promhttp.Handler()))
}
