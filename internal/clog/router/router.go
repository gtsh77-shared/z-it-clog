package router

import (
	"github.com/labstack/echo/v4"
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/handler/health"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/handler/ipv4"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/storage"
	"go.uber.org/zap"
)

func New(
	l *zap.SugaredLogger,
	c *config.Config,
	st *storage.Storage) (*echo.Echo, error) {
	var (
		e    *echo.Echo  = echo.New()
		base *echo.Group = e.Group("")

		err error
	)

	e.HideBanner = true

	//common midware (cors, timeout)
	EnableMidWare(e, c)

	//custom prom metrics
	EnableMetrics(e, c.Net.MetricPath)

	//custom json validation
	if err = EnableCustomValidation(e); err != nil {
		return nil, err
	}

	//handlers base
	health := health.New(l, c)
	ipv4 := ipv4.New(l, c, st)

	base.GET(c.Net.HealthPath, health.Info)
	base.GET("/:uid1/:uid2", ipv4.IsDup)

	return e, nil
}
