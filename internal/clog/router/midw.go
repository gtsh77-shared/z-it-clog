package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
)

func EnableMidWare(
	e *echo.Echo,
	config *config.Config) {
	//cors
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		Skipper:      middleware.DefaultSkipper,
		AllowOrigins: config.Net.DomainNames,
		AllowMethods: []string{http.MethodGet},
	}))

	//timeout
	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		OnTimeoutRouteErrorHandler: nil,

		Skipper: middleware.DefaultSkipper,
		Timeout: config.Net.HTTPTimeout,
	}))
}
