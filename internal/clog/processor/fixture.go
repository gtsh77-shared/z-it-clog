package processor

import (
	"bytes"
	"fmt"
	"math/rand"
	"os"
	"runtime"
	"time"

	"gitlab.com/gtsh77-shared/z-it-clog/pkg/tools"
)

func (r *Processor) applyFixture() {
	var (
		m runtime.MemStats
		t time.Time = time.Now()
		b bytes.Buffer

		fdata []byte
		err   error
	)

	r.l.Named(TickerID).Info("generating fixtures...")

	for i := 0; i < r.c.Processor.FixtureLinesCount; i++ {
		b.WriteString(fmt.Sprint(
			uint32(rand.Intn(r.c.Processor.FixtureUsersCount-1)+1), //nolint:gosec //ack
			",",
			tools.Inet_ntoa(uint32(rand.Intn(1e5-1)+1)).String(), //nolint:gosec //ack
			",",
			time.Now().Format(time.TimeOnly),
			"\n"))
	}

	fdata = b.Bytes()
	if err = os.WriteFile(r.c.Processor.FixtureFilePath, fdata, 0600); err != nil {
		r.l.Error(err)
	}

	runtime.ReadMemStats(&m)
	r.l.Named(TickerID).Infof("fixture-lines generated: %d heap: %d MB, time: %.2fs", r.c.Processor.FixtureLinesCount, m.HeapAlloc/1024/1024, time.Since(t).Seconds())
	r.l.Named(TickerID).Infof("file overwritten: %s, bytes: %d", r.c.Processor.FixtureFilePath, len(fdata))
}
