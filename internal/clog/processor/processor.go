package processor

import (
	"bufio"
	"flag"
	"fmt"
	"io/fs"
	"net"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/puzpuzpuz/xsync"
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/storage"
	"gitlab.com/gtsh77-shared/z-it-clog/pkg/ticker"
	"gitlab.com/gtsh77-shared/z-it-clog/pkg/tools"
	"go.uber.org/zap"
)

const (
	TickerID        = "file-processor"
	DataLineElemCnt = 3
)

type Processor struct {
	l  *zap.SugaredLogger
	c  *config.Config
	t  *ticker.Ticker
	f  *os.File
	st *storage.Storage
	mu xsync.RBMutex
	ps chan<- int

	isLoaded bool

	offset, size, total int64
}

func NewProcessor(
	l *zap.SugaredLogger,
	c *config.Config,
	st *storage.Storage,
	ps chan<- int) *Processor {
	return &Processor{
		l:  l,
		c:  c,
		st: st,
		ps: ps,
	}
}

func (r *Processor) Start() (*Processor, error) {
	var (
		path string
		err  error
	)

	if flag.Lookup("test.v") == nil {
		path = r.c.Processor.FilePath
	} else {
		path = r.c.Processor.FilePathAt
	}

	if r.f, err = os.Open(path); err != nil {
		return nil, err
	}

	r.t = ticker.New(
		TickerID,
		r.c.Processor.PollInterval,
		r.proccess,
		r.except)

	if r.c.Processor.FixtureEnabled {
		r.applyFixture()
	}

	return r, nil
}

func (r *Processor) Watch() {
	r.t.Enable()
}

func (r *Processor) UnWatch() {
	r.t.Disable()
}

func (r *Processor) proccess() error {
	var (
		fi  fs.FileInfo
		err error
	)

	if fi, err = r.f.Stat(); err != nil {
		return err
	}

	if fi.Size() > r.size {
		if err = r.readLines(); err != nil {
			return err
		}
	}

	if !r.isLoaded {
		r.ps <- 1
		r.isLoaded = true
	}

	return nil
}

func (r *Processor) except(err error) {
	r.l.Named(TickerID).Warn(err)
}

func (r *Processor) readLines() error {
	var (
		t   time.Time = time.Now()
		m   runtime.MemStats
		sc  *bufio.Scanner
		d   string
		cnt int64
		err error
	)

	r.mu.Lock()
	defer r.mu.Unlock()

	if _, err = r.f.Seek(r.offset, 0); err != nil {
		return err
	}

	sc = bufio.NewScanner(r.f)

	for i := 0; sc.Scan() && i < r.c.Processor.BatchSize; i++ {
		d = sc.Text()
		r.size += int64(len(sc.Bytes()) + 1)
		r.offset += int64(len(d) + 1)

		if err = r.processLine(d); err != nil {
			r.l.Named("TickerID").Warn(err)
		}
		cnt++
	}

	r.total += cnt
	runtime.ReadMemStats(&m)
	r.l.Named(TickerID).Infof("lines-processed: %d, total: %d, heap: %d MB, time: %.2fs", cnt, r.total, m.HeapAlloc/1024/1024, time.Since(t).Seconds())

	return nil
}

func (r *Processor) processLine(data string) error {
	var (
		s   []string
		t   time.Time
		id  int
		ip  net.IP
		err error
	)

	s = strings.Split(data, ",")
	for i, v := range s {
		s[i] = strings.TrimSpace(v)
	}

	if len(s) < DataLineElemCnt {
		return fmt.Errorf("invalid string: %s", data)
	}

	if id, err = strconv.Atoi(s[0]); err != nil {
		return err
	}

	if ip = net.ParseIP(s[1]); ip == nil {
		return fmt.Errorf("invalid ipv4 format: %s", s[1])
	}

	if t, err = time.Parse(time.TimeOnly, s[2]); err != nil {
		return err
	}

	if err = r.st.AddTS(uint32(id), tools.Inet_aton(ip), t.Unix()); err != nil {
		return err
	}

	return nil
}
