package service

import (
	"context"
	"flag"

	"github.com/labstack/echo/v4"
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/processor"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/storage"
	"gitlab.com/gtsh77-shared/z-it-clog/pkg/tools"
	"go.uber.org/zap"
)

const (
	ServicePrefix = "CLOG"
	IDLenBytes    = 3
)

type Service struct {
	Conf *config.Config
	ctx  context.Context
	cf   context.CancelFunc
	log  *zap.SugaredLogger
	st   *storage.Storage
	pr   *processor.Processor

	T      *echo.Echo
	ss, ps chan int
}

type Servicer interface {
	recover()
	gracefulShutdown()
	Start() error
	ReadENV() error
	StartLogger() error
	StartRouter() error
}

func New(f1, f2, f3 string) *Service {
	return &Service{
		Conf: config.New(f1, f2, f3),
		ss:   make(chan int),
		ps:   make(chan int),
	}
}

func (s *Service) Start() (*Service, error) {
	var err error

	defer s.recover()
	go s.gracefulShutdown()

	//parent ctx
	s.ctx, s.cf = context.WithCancel(context.Background())
	defer s.cf()

	//parse env-args
	if err = s.ReadENV(); err != nil {
		return nil, err
	}

	//start custom logger
	if err = s.StartLogger(); err != nil {
		return nil, err
	}

	//define ipv4 by i-face name && index
	if s.Conf.Net.Addr, err = tools.NetAddrOfInterface(s.Conf.Net.InterfaceName, s.Conf.Net.InterfaceAddrIndex); err != nil {
		s.log.With("network", err).Fatal()
	}

	//generate random instance id
	if s.Conf.Runtime.ID, err = tools.RandHex(IDLenBytes); err != nil {
		s.log.With("id", err).Warn()
	}

	//start processor
	if err = s.StartProcessor(); err != nil {
		s.log.With("processor-start-failed", err).Fatal()
	}

	//wait for processor init semaphore
	<-s.ps

	//start http(s) router and server
	if err = s.StartRouter(); err != nil {
		s.log.With("router-start-failed", err).Fatal()
	}

	if flag.Lookup("test.v") == nil {
		//wait for stop semaphore
		<-s.ss
	}

	return s, nil
}
