package service

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"github.com/ardanlabs/conf/v3"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/processor"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/router"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/storage"
	"gitlab.com/gtsh77-shared/z-it-clog/pkg/tools"
	"go.uber.org/zap/zapcore"
)

func (s *Service) recover() {
	var (
		rec  interface{}
		buf  [2048]byte
		blen int
	)

	if rec = recover(); rec != nil {
		blen = runtime.Stack(buf[:], true)
		s.log.Error("runtime panic")
		os.Stdout.Write(buf[:blen])
	}
}

func (s *Service) gracefulShutdown() {
	var (
		gs  chan os.Signal = make(chan os.Signal, 1)
		sig os.Signal
		err error
	)

	signal.Notify(gs,
		syscall.SIGTERM,
		syscall.SIGINT,
		syscall.SIGSEGV,
		syscall.SIGBUS,
		syscall.SIGILL,
		syscall.SIGSYS)

	sig = <-gs

	s.log.Warnf("graceful shutdown: %s", sig.String())

	if err = s.T.Shutdown(s.ctx); err != nil {
		s.log.Error(err)
	}

	s.ss <- 1
	os.Exit(0)
}

func (s *Service) StartLogger() error {
	var err error

	if s.log, err = tools.NewLogger(&tools.LoggerConfig{
		Level:         zapcore.Level(s.Conf.Runtime.LogLevel),
		IsJSONEncoder: s.Conf.Runtime.LogAsJSON,
	}); err != nil {
		return err
	}

	return err
}

func (s *Service) ReadENV() error {
	var (
		h   string
		err error
	)

	if h, err = conf.Parse(ServicePrefix, s.Conf); err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(h) //nolint:forbidigo //non-json ouptput required
			return nil
		}

		return err
	}

	return nil
}

func (s *Service) StartProcessor() error {
	var err error

	s.st = storage.NewStorage()
	s.pr = processor.NewProcessor(s.log, s.Conf, s.st, s.ps)

	if _, err = s.pr.Start(); err != nil {
		return err
	}

	s.pr.Watch()

	return nil
}

func (s *Service) StartRouter() error {
	var (
		addr string
		err  error
	)

	if s.T, err = router.New(s.log, s.Conf, s.st); err != nil {
		return err
	}

	s.T.Server.BaseContext = func(l net.Listener) context.Context {
		return s.ctx
	}

	addr = fmt.Sprint(s.Conf.Net.Addr.String(), ":", s.Conf.Net.HTTPPort)

	go func() {
		if s.Conf.Net.TLS.Enabled {
			if err = s.T.StartTLS(addr, s.Conf.Net.TLS.CrtPath, s.Conf.Net.TLS.KeyPath); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					s.log.Warn(err)
				}
			}
		} else {
			if err = s.T.Start(addr); err != nil {
				s.log.Warn(err)
			}
		}
	}()

	return nil
}
