package ipv4

type IsDupJSON struct {
	V bool `json:"dupes"`
}
