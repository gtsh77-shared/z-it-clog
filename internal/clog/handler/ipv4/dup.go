package ipv4

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) IsDup(c echo.Context) error {
	var (
		d1, d2 int
		isDup  bool
		err    error
	)

	if d1, err = strconv.Atoi(c.Param("uid1")); err != nil {
		h.L.Named(ClogGrp).Error(err)
		return c.JSON(http.StatusBadRequest, "Bad request")
	}

	if d2, err = strconv.Atoi(c.Param("uid2")); err != nil {
		h.L.Named(ClogGrp).Error(err)
		return c.JSON(http.StatusBadRequest, "Bad request")
	}

	if d1 != d2 {
		isDup = h.storage.IsDupIPv4(uint32(d1), uint32(d2))
	} else {
		isDup = true
	}

	//return json
	return c.JSON(http.StatusOK, &IsDupJSON{
		V: isDup,
	})
}
