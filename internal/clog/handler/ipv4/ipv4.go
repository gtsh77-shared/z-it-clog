package ipv4

import (
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/handler"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/storage"
	"go.uber.org/zap"
)

const (
	ClogGrp = "clog-handler"
)

type Handler struct {
	*handler.BaseHandler
	storage *storage.Storage
}

func New(
	l *zap.SugaredLogger,
	c *config.Config,
	st *storage.Storage) *Handler {
	return &Handler{
		BaseHandler: handler.New(c, l),
		storage:     st,
	}
}
