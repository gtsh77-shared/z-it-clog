package ipv4_test

import (
	"net/http"
	"testing"

	"gitlab.com/gtsh77-shared/z-it-clog/pkg/autotest"
)

func TestDupCheck(t *testing.T) {
	var at *autotest.AT = autotest.New(&autotest.ATConfig{
		AuthGroup: false,
	})

	cases := []*autotest.TestCase{
		{
			Description:      "try to get not exists ids",
			URL:              "/111/222",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":false}`,
		},
		{
			Description:      "try to get ident ids",
			URL:              "/1/1",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":true}`,
		},
		{
			Description:      "try to get dup seq 1",
			URL:              "/1/2",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":true}`,
		},
		{
			Description:      "try to get dup seq 1 (reverse)",
			URL:              "/2/1",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":true}`,
		},
		{
			Description:      "try to get dup seq 2",
			URL:              "/1/3",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":false}`,
		},
		{
			Description:      "try to get dup seq 2 (reverse)",
			URL:              "/3/1",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":false}`,
		},
		{
			Description:      "try to get dup seq 3",
			URL:              "/2/3",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":true}`,
		},
		{
			Description:      "try to get dup seq 3 (reverse)",
			URL:              "/3/2",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":true}`,
		},
		{
			Description:      "try to get dup seq 4",
			URL:              "/1/4",
			Method:           http.MethodGet,
			ExpectedStatus:   http.StatusOK,
			ExpectedResponse: `{"dupes":false}`,
		},
	}

	for index, c := range cases {
		at.DoTest(index, t, c)
	}
}
