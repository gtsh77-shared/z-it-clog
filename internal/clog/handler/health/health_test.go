package health_test

import (
	"net/http"
	"testing"

	"gitlab.com/gtsh77-shared/z-it-clog/pkg/autotest"
)

const (
	HCPath = "/health/check"
)

func TestHealthCheck(t *testing.T) {
	var at *autotest.AT = autotest.New(&autotest.ATConfig{
		AuthGroup: false,
	})

	cases := []*autotest.TestCase{
		{
			Description:    "try to get health check info",
			URL:            HCPath,
			Method:         http.MethodGet,
			ExpectedStatus: http.StatusOK,
			MustHaveJSONFields: map[string]interface{}{
				"version":     "",
				"compiled_at": "",
			},
		},
	}

	for index, c := range cases {
		at.DoTest(index, t, c)
	}
}
