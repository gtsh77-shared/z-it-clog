package health

type HealthJSON struct {
	ID         string `json:"runtime_id"`
	Version    string `json:"version"`
	CompiledAt string `json:"compiled_at"`
}
