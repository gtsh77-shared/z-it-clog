package health

import (
	"net/http"

	"github.com/labstack/echo/v4"
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"gitlab.com/gtsh77-shared/z-it-clog/internal/clog/handler"
	"go.uber.org/zap"
)

type Handler struct {
	*handler.BaseHandler
}

func New(
	l *zap.SugaredLogger,
	c *config.Config) *Handler {
	return &Handler{
		BaseHandler: handler.New(c, l),
	}
}

func (h *Handler) Info(c echo.Context) error {
	return c.JSON(http.StatusOK, &HealthJSON{
		ID:         h.C.Runtime.ID,
		Version:    h.C.Runtime.Version,
		CompiledAt: h.C.Runtime.CompiledAt,
	})
}
