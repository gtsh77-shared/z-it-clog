package handler

import (
	config "gitlab.com/gtsh77-shared/z-it-clog/config/clog"
	"go.uber.org/zap"
)

type BaseHandler struct {
	C *config.Config
	L *zap.SugaredLogger
}

func New(
	c *config.Config,
	l *zap.SugaredLogger) *BaseHandler {
	return &BaseHandler{
		L: l,
		C: c,
	}
}
