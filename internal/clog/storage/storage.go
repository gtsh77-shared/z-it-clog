package storage

import (
	"math"

	"github.com/puzpuzpuz/xsync"
	"gitlab.com/gtsh77-shared/z-it-clog/pkg/rbt"
)

type Storage struct {
	rbt *rbt.RBT
	mu  xsync.RBMutex
}

type Storager interface {
	Load(uint32) (*xsync.MapOf[uint32, []int64], bool)
	Store(uint32, *xsync.MapOf[uint32, []int64])
	AddTS(uint32, uint32, int64) error
	IsDupIPv4(uint32, uint32) bool
	addUser(uint32, uint32, int64)
	updateUser(*xsync.MapOf[uint32, []int64], uint32, int64)
}

func NewStorage() *Storage {
	return &Storage{
		rbt: rbt.NewRBT(),
	}
}

func (s *Storage) Load(uid uint32) (*xsync.MapOf[uint32, []int64], bool) {
	var (
		n1 *interface{}
		n2 *xsync.MapOf[uint32, []int64]

		ok bool
	)

	if _, n1, ok = s.rbt.Search(uid); !ok {
		return nil, false
	} else {
		if n2, ok = (*n1).(*xsync.MapOf[uint32, []int64]); !ok {
			return nil, false
		}
	}

	return n2, true
}

func (s *Storage) Store(uid uint32, m *xsync.MapOf[uint32, []int64]) {
	s.rbt.Add(uid, m)
}

func (s *Storage) AddTS(uid, ipv4 uint32, ts int64) error {
	var (
		m  *xsync.MapOf[uint32, []int64]
		ok bool
	)

	s.mu.Lock()
	defer s.mu.Unlock()

	if m, ok = s.Load(uid); !ok {
		s.addUser(uid, ipv4, ts)
	} else {
		s.updateUser(m, ipv4, ts)
	}

	return nil
}

func (s *Storage) IsDupIPv4(uid1, uid2 uint32) bool {
	var (
		m1, m2 *xsync.MapOf[uint32, []int64]

		ts        []int64
		tsv       int64
		isDup, ok bool
	)

	if m1, ok = s.Load(uid1); !ok {
		return false
	}

	if m2, ok = s.Load(uid2); !ok {
		return false
	}

	m1.Range(func(k uint32, v []int64) bool {
		if ts, ok = m2.Load(k); ok {
			for _, tsv = range v {
				if s.bSearch(ts, tsv) != -1 {
					isDup = true

					return false
				}
			}
		}

		return true
	})

	return isDup
}

func (s *Storage) addUser(uid, ipv4 uint32, ts int64) {
	var (
		m *xsync.MapOf[uint32, []int64]
		t []int64
	)

	t = []int64{ts}
	m = xsync.NewIntegerMapOf[uint32, []int64]()
	m.Store(ipv4, t)
	s.Store(uid, m)
}

func (s *Storage) updateUser(m *xsync.MapOf[uint32, []int64], ipv4 uint32, ts int64) {
	var (
		t  []int64
		ok bool
	)

	if t, ok = m.Load(ipv4); !ok {
		t = []int64{ts}
	} else {
		t = append(t, ts)
	}

	m.Store(ipv4, t)
}

func (s *Storage) bShift(data []int64, l int64, k int64) int64 {
	for i := l; i > 0; i-- {
		if data[i] != k {
			return i
		}
	}

	return l
}

func (s *Storage) bSearch(data []int64, k int64) int64 {
	var (
		l, r int64 = -1, int64(len(data))
		m    int64
	)

	for r-l > 1 {
		m = int64(math.Floor(float64((l + r) / 2))) //nolint: staticcheck //ack
		if data[m] < k {
			l = m
		} else {
			r = m
		}
	}

	if r < int64(len(data)) && data[r] == k {
		if r != 0 && data[r-1] == k {
			return s.bShift(data, r-1, k)
		}
		return r
	}

	return -1
}
